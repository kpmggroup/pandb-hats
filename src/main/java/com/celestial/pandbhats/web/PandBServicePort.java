/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.pandbhats.web;

import java.util.ArrayList;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;


import com.celestial.pandbhats.utils.SimpleJsonMessage;
import com.celestial.pandbhats.core.LoginController;
import java.util.logging.Level;


/**
 *
 * @author Selvyn
 */
@Path("/services")
public class PandBServicePort implements IPandBServicePort
{
    @Override
    @GET
    @Path("/sayhello")
    public Response sayHtmlHelloTest()
    {
        String result = "<html> " + "<title>" + "hanc" + "</title>"
                + "<body><h1>" + "pandhats is running..." + "</h1></body>" + "</html> ";

        return Response.status(200).entity(result).build();
    }
    
    @Override
    @POST
    @Path("/login")
    public Response loginWithInfoFromForm( @FormParam("who")String who,
                                        @FormParam("pwd")String pwd)
    {
        LoginController loginCtrl = new LoginController();
        if( loginCtrl.loginUser(who, pwd) )
        {
            Logger.getGlobal().log(Level.INFO, who + "//" +"// is now logged in");
            return Response.status(200).entity( new SimpleJsonMessage( who + "//" +"// is now logged in" ) ).build();
        }
        else
        {
            Logger.getGlobal().log(Level.INFO, who + "//" +"// is NOT logged in");
            return Response.status(404).entity( new SimpleJsonMessage( who + "//" +"// is NOT logged in" ) ).build();
        }            
    }
}
