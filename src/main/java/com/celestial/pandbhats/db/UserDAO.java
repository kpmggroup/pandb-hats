package com.celestial.pandbhats.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.sql.CallableStatement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAO
{
    Connection con = null;

    private static String userTableName = "users";

    public boolean loginUser(String name, String pwd) throws SQLException
    {
        boolean result = false;
        try
        {
            con = DBConnector.getConnector().getConnection();
            Statement st = con.createStatement();
            String stmnt = "select * from " + userTableName + " where ";
            stmnt += userTableName + ".user_id = '" + name + "'";
            stmnt += " and " + userTableName + ".user_pwd = '" + pwd + "'";
            ResultSet rs = st.executeQuery(stmnt);
            while (rs.next())
            {
                result = true;
            }
        } catch (SQLException se)
        {
            System.out.println(se);
        } catch (IOException ex)
        {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
